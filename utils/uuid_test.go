package utils

import "testing"

/**
 * 获取UUID值
 */
func TestGetUUID(t *testing.T) {
	uuid, e := GetUUID()

	if e != nil {
		t.Logf("Error: %s", e.Error())
	}

	t.Logf("UUID: %s", uuid)
}