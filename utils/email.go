package utils

import (
	"log"

	"gopkg.in/gomail.v2"
)

/**
 * GoMail Send To User(GoMail 发送邮件到用户)
 */
type GoMailToUser struct {
	Name    string
	Address string
}

/**
 * GoMail Send From User(GoMail 发送邮件用户)
 */
type GoMailFromUser struct {
	Name    string
	Address string
}

/**
 * GoMail Send To Users List(GoMail 发送邮件到用户列表)
 */
type GoMailToList struct {
	To []*GoMailToUser
}

/**
 * GoMail STMPServer Config Options(GoMail SMTP服务器配置信息结构)
 */
type GoMailConfig struct {
	STMPServer        string
	STMPPort          int
	STMPLoginUsername string
	STMPLoginPassword string
}

/**
 * 使用GoMail发送邮件
 */
func SendEmailForGoMail(GoMailConfig *GoMailConfig, from *GoMailFromUser, to *GoMailToList, subject, content string) error {
	// GoMail 拨号器
	dialer := gomail.NewDialer(
		GoMailConfig.STMPServer,
		GoMailConfig.STMPPort,
		GoMailConfig.STMPLoginUsername,
		GoMailConfig.STMPLoginPassword,
	)

	// 拨号并对SMTP服务器进行身份验证。返回SendCloser
	sendCloser, e := dialer.Dial()

	// 拨号错误处理
	if e != nil {
		return e
	}

	// NewMessage创建一个新消息。它使用UTF-8和可引用打印编码
	message := gomail.NewMessage()

	for _, t := range to.To {
		// 邮件发件人, 邮箱地址
		message.SetAddressHeader("From", from.Address, from.Name)

		// 邮件收件人, 邮箱地址
		message.SetAddressHeader("To", t.Address, t.Name)

		// 邮件主题
		message.SetHeader("Subject", subject)

		// 邮件内容
		message.SetBody("text/html", content)

		// 发送邮件
		e := gomail.Send(sendCloser, message)

		// 发送邮件错误处理
		if e != nil {
			// %q: 使用Go语法安全地转义单引号字符文字
			// %v: 使用类型默认格式的值
			log.Printf("Could not send email to %q: %v", t.Address, e)
		} else {
			log.Printf("Email send successful! to :%q", t.Address)
		}

		// 重置邮件消息
		message.Reset()
	}

	return nil
}
