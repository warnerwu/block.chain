package utils

/**
 * 获取用户注册成功后的激活地址URL
 *
 * @param username string	用户名
 * @param email string 激活地址
 * @param session string session
 * @return string 用户激活地址
 */
func GetUserActivateUrl(username, email, session string) string {
	return "http://127.0.0.1:9190/user-register-successful/activate/" + GetBase64Encoding(username) + "/" + GetBase64Encoding(email) + "/" + session
}
