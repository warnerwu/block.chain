package utils

/**
 * 获取用户注册成功邮件模板
 *
 * @param username string	用户名
 * @param activateUrl string 激活地址
 * @return string 注册成功邮件模板字符串
 */
func GetUserRegisteredSuccessful(username, activateUrl string) string {
	return `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>
<body class=" sidebar-collapse">
<div class="wrapper" style="color: white;">
    <!-- Start 内容区块 -->
    <div class="section" style="margin: 2rem auto;">
        <div style="min-height: 700px; margin-right: auto; margin-left: auto; padding-right: 15px; padding-left: 15px; max-width: 1000px">
            <div style="display: -ms-flexbox;display: flex;-ms-flex-wrap: wrap;flex-wrap: wrap;margin-right: -15px;margin-left: -15px">
                <div style="-ms-flex: 0 0 100%;flex: 0 0 100%;max-width: 100%; min-height: 650px;">
                    <div style="text-align: center!important;padding: 2rem 1rem;margin-bottom: 2rem;background: url(https://timgsa.baidu.com/timg?image&quality=80&size=b10000_10000&sec=1532864227&di=5d13c9c07b557fe4b545ab7d55e46996&src=http://pic1.win4000.com/wallpaper/c/548a92daa8a60.jpg) top left no-repeat; background-size: cover; border-radius: .3rem;min-height: 650px;">
                        <h4 style="margin: 6rem auto;">用户名为【 <i style="color: #E76C42;">` + username + `</i> 】的账户注册成功！</h4>
                        <h5 style="margin: 6rem auto;">请点击下面的激活按钮进行激活账号</h5>
                        <p>
                            <a style="opacity: .65; box-shadow: 0 0 0 3px rgba(0,255,64,0.5);padding: .5rem 1rem;font-size: 1.25rem;line-height: 1.5;border-radius: .3rem; color: white; text-decoration: none;" href="` + activateUrl + `" role="button">立即激活账号</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>`
}
