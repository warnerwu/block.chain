package utils

import (
	"log"
	"time"
)

/**
包含常用时间格式化字符串
*/

const (
	china         = "2006-01-02 03:04:05"        // 中国常用时间格式
	china24       = "2006-01-02 15:04:05"        // 中国常用时间格式, 24小时制
	chinaStatus   = "2006-01-02 03:04:05 pm"     // 中国常用时间格式, 带有时间状态
	china24Status = "2006-01-02 15:04:05 pm"     // 中国常用时间格式, 带有时间状态, 24小时制
	usa           = "Jan 02 2006 03:04:05"       // 美国西方常用时间格式
	usa24         = "Jan 02 2006 15:04:05"       // 美国西方常用时间格式, 24小时制
	usaStatus     = "Jan 02 2006 03:04:05 pm"    // 美国西方常用时间格式, 带有时间状态
	usa24Status   = "Jan 02 2006 15:04:05 pm"    // 美国西方常用时间格式, 带有时间状态, 24小时制
	timezone      = "Asia/Shanghai"              // 时区定义
	UTC           = "02/Jan/2006:15:04:05 +0800" // UTC 时间格式
)

/**
获取当前时间格式化字符串

@return string 当前时间格式化字符串
*/
func GetNowTimeFormatString() string {
	return time.Unix(GetTimestamp(), 0).Format(china24)
}

/**
转递时间戳返回格式化日期时间字符串

@param timestamp int64 时间戳
@return string 格式化时间格式
*/
func GetFormatTimeString(timestamp int64, format string) string {
	formatString := ""
	switch format {
	case "china":
		formatString = china
	case "china24":
		formatString = china24
	case "chinaStatus":
		formatString = chinaStatus
	case "china24Status":
		formatString = china24Status
	case "usa":
		formatString = usa
	case "usa24":
		formatString = usa24
	case "usaStatus":
		formatString = usaStatus
	case "usa24Status":
		formatString = usa24Status
	}
	return time.Unix(timestamp, 0).Format(formatString)
}

/**
获取当前时间戳

@return timestamp int64
*/
func GetTimestamp() (timestamp int64) {
	return time.Now().Unix()
}

/**
转换字符串时间到日期时间格式

@param t string 日期时间字符串 30/Jun/2018:00:25:11 +0800
@return time.Time
*/

func GetFormatString2Time(t string) time.Time {
	// 获取到本地时区
	location, _ := time.LoadLocation(timezone)

	// 解析字符串日期到指定格式日期时间
	inLocation, e := time.ParseInLocation(UTC, t, location)

	// 错误处理
	if e != nil {
		log.Printf("ParseInLocation fail of the UTC:%s", e.Error())
	}

	// 返回日期时间
	return inLocation
}

/**
 *生成过期时间
 *
 *@param duration int 多长时间过期
 *@param accuracy string 时间类型, 如: [day|hour|minute|second]
 *@return int64 Session过期时间
 */
func GetExpireTimestamp(duration int64, accuracy string) int64 {
	// 当前时间戳
	cur := time.Now().Unix()

	// 匹配过期时间类型
	switch accuracy {
	case "day":
		cur += duration * 60 * 60 * 24
	case "hour":
		cur += duration * 60 * 60
	case "minute":
		cur += duration * 60
	case "second":
		cur += duration
	}

	// 返回过期时间
	return cur
}
