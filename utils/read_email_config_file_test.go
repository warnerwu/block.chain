package utils

import "testing"

/**
 * 测试 - 读取【邮件配置】信息
 */
func TestReadEmailConfigJsonFile(t *testing.T) {
	// 调用方法
	config, e := ReadEmailConfigJsonFile("./../config/own.email.config.json")

	// 错误检测
	if e != nil {
		t.Logf("read config file error: %+v", config)
	}

	// 输出读取【邮件配置】信息
	t.Logf("read config file content value: %+v", config.STMP)
}
