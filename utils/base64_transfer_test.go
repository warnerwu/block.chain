package utils

import "testing"

var encodingUsername string
var decodingUsername string

func TestBase64WorkFlow(t *testing.T) {
	// SubTest
	t.Run("testGetBase64Encoding", testGetBase64Encoding)
	t.Run("testGetBase64Decoding", testGetBase64Decoding)
}

/**
 * Base64字符串加密
 */
func testGetBase64Encoding(t *testing.T) {
	encodingUsername = GetBase64Encoding("warnerwu@126.com")

	t.Logf("GetBase64Encoding: %s", encodingUsername)
}

/**
 * Base64字符串解密
 */
func testGetBase64Decoding(t *testing.T) {
	decodingUsername = GetBase64Decoding(encodingUsername)

	t.Logf("GetBase64Encoding: %s", decodingUsername)
}
