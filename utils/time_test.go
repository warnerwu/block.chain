package utils

import (
	"fmt"
	"testing"
)

/**
测试获取当前时间戳
*/
func TestTimestamp(t *testing.T) {
	timestamp := GetTimestamp()
	t.Logf("timestamp is :%v\n", timestamp)
}

/**
测试获取当前当前格式化格式
*/

func TestTimeString(t *testing.T) {

	// 中国标准时间
	china := GetFormatTimeString(GetTimestamp(), "china")
	// 日志输出~中国标准时间
	t.Logf("format time string is:%v\n", china)

	// 中国标准时间,24小时制
	china24 := GetFormatTimeString(GetTimestamp(), "china24")
	// 日志输出~中国标准时间
	t.Logf("format time string is:%v\n", china24)
}

/**
获取当前时间测试
*/
func TestGetNowTimeFormatString(t *testing.T) {
	// 获取当前时间戳格式化字符串
	nowTimeFormatString := GetNowTimeFormatString()

	// 日志输出~获取当前时间戳格式化字符串
	t.Logf("Now time format string is:%v", nowTimeFormatString)
}

func TestGetFormatString2Time(t *testing.T) {
	timeStr := "30/Jun/2018:00:25:11 +0800"
	t.Logf("GetFormatString2Time: %v", GetFormatString2Time(timeStr))
}

func TestGetExpireTimestamp(t *testing.T) {

	timestamp := GetExpireTimestamp(120, "second")
	fmt.Println(timestamp)

	timestamp = GetExpireTimestamp(5, "minute")
	fmt.Println(timestamp)

	timestamp = GetExpireTimestamp(2, "hour")
	fmt.Println(timestamp)

	timestamp = GetExpireTimestamp(7, "day")
	fmt.Println(timestamp)
}
