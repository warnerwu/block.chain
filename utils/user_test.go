package utils

import (
	"testing"
)

/**
 * 获取用户注册成功后的激活地址URL
 */
func TestGetUserActivateUrl(t *testing.T) {
	uuid, _ := GetUUID()
	url := GetUserActivateUrl("king", "king@gamil.com", uuid)
	t.Logf("url is value: %s", url)
}
