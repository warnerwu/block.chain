package utils

const (
	MediaCateOfMovie = "movie" // 流媒体类型 ~ 电影
	MediaCateOfMV    = "mv"    // 流媒体类型 ~ MV
)

/**
转递流媒体分类字符串返回整数类型流媒体分类ID

@param mediaCateString string 流媒体分类字符串
@return int 整数类型流媒体分类ID
*/
func GetMediaCateIntIdForString(mediaCateString string) int {
	switch mediaCateString {
	case MediaCateOfMovie: // 电影
		return 1
	case MediaCateOfMV: // MV
		return 2
	default: // 未知分类
		return 0
	}
}
