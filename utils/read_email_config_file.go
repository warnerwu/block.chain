package utils

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

/**
 * 【邮件配置】结构体定义
 */
type EmailConfig struct {
	// STMP相关配置信息
	STMP struct {
		Server        string
		Port          int
		LoginUsername string
		LoginPassword string
	}
}

/**
 * 读取【邮件配置】信息
 *
 * @param filePath string JSON配置文件路径
 * @return *EmailConfig, error 邮件配置结构体数据以及错误处信息
 */
func ReadEmailConfigJsonFile(filePath string) (*EmailConfig, error) {
	// 初始化【邮件】配置结构体
	config := EmailConfig{}

	// 读取【邮件配置】文件
	bytes, e := ioutil.ReadFile(filePath)

	// 读取【邮件配置】错误检测
	if e != nil {
		// 输出调试信息, 便于调试
		log.Printf("load config file error of the own.email.config.json, error detail:%s", e.Error())

		// 返回空配置, 以及错误信息
		return nil, e
	}

	// 将读取到的JSON【配置字节数据】转换为结构体数据
	if e := json.Unmarshal(bytes, &config); e != nil {
		// 输出调试信息, 便于调试
		log.Printf("transition file content to struct error")

		// 返回空配置, 以及错误信息
		return nil, e
	}

	// 返回读取到的JSON到结构体数据, 以及无错误
	return &config, nil
}
