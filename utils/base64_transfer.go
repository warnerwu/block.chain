package utils

import "encoding/base64"

/**
 * 获取字符串Base64编码字符串
 *
 * @param s string 输入字符串
 * @return string Base64编码之后字符串
 */
func GetBase64Encoding(s string) string {
	return base64.StdEncoding.EncodeToString([]byte(s))
}

/**
 * 获取字符串Base64编码字符串
 *
 * @param s string 输入字符串
 * @return string Base64编码之后字符串
 */
func GetBase64Decoding(s string) string {
	// 解码Base64字符串
	bytes, _ := base64.StdEncoding.DecodeString(s)
	// 返回解码后的字符串
	return string(bytes)
}
