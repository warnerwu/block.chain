package utils

import "testing"

/**
 * 转递流媒体分类字符串返回整数类型流媒体分类ID
 */
func TestGetMediaCateIdForString(t *testing.T) {
	mediaCateId := GetMediaCateIntIdForString("mv")
	t.Logf("media cate id is: %d", mediaCateId)
}
