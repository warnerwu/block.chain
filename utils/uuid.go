package utils

import (
	"crypto/rand"
	"fmt"
	"io"
)

/**
获取UUID值
*/
func GetUUID() (string, error) {
	// 定义长度为 16, 类型为 []byte 的切片 bytes
	bytes := make([]byte, 16)

	// ReadFull 精确地从 rand.Reader 中将 len(bytes) 个字节读取到 bytes 中
	n, err := io.ReadFull(rand.Reader, bytes)

	// 错误处理
	if err != nil || n != len(bytes) {
		return "", err
	}

	// 第七个节点
	// variant bits; see section 4.1.1
	bytes[8] = bytes[8]&^0xc0 | 0x80
	// 第五个节点
	// version 4 (pseudo-random); see section 4.1.3
	bytes[6] = bytes[6]&^0xf0 | 0x40

	// 返回UUID
	return fmt.Sprintf("%x-%x-%x-%x-%x", bytes[0:4], bytes[4:6], bytes[6:8], bytes[8:10], bytes[10:]), nil
}
