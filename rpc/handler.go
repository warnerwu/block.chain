package main

import (
	"encoding/json"
	"io"
	"net/http"

	"block.chain/core"
	"github.com/julienschmidt/httprouter"
)

/**
 * 定义全局变量
 */
var blockChain = core.NewBlockChain()

/**
 * 状态码全局定义 - 服务器内部错误
 */
const StatusInternalServerError = http.StatusInternalServerError

/**
 * 状态码全局定义 - 请求成功
 */
const statusCode = http.StatusOK

/**
 *获取【区块链】
 *
 *@params w http.ResponseWriter
 *@params r *http.Response
 *@params p httprouter.Params
 */
func GetBlockChain(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	// 转换结构体数据到JSON字节切片
	blockChainByte, err := json.Marshal(blockChain)

	// 转换结构体数据到JSON字节切片 ~ 存在错误
	if err != nil {
		// 写入【响应头HTTP状态码】
		w.WriteHeader(StatusInternalServerError)

		// 返回错误响应
		http.Error(w, err.Error(), StatusInternalServerError)

		// 直接返回
		return
	}

	// 写入Header头响应信息状态码数据
	w.WriteHeader(statusCode)

	// 写入响应信息内容
	io.WriteString(w, string(blockChainByte))
}

/**
 *添加【区块】到【区块链】
 *
 *@params w http.ResponseWriter
 *@params r *http.Response
 *@params p httprouter.Params
 */
func AddBlockIntoBlockChain(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	// 获取待生成区块的【区块体】数据
	blockData := params.ByName("block-data")

	// 发送数据生成【区块】并添加到【区块链】
	blockChain.SendData(blockData)

	// 再将生成后的数据发送到客户端
	GetBlockChain(w, r, params)
}
