package main

import "github.com/julienschmidt/httprouter"

/**
Register Routers
*/
func RegisterFunctions() *httprouter.Router {
	// instance http router
	router := httprouter.New()

	// Create User(用户注册)
	router.GET("/block-chains", GetBlockChain)

	// User Login(用户登录)
	router.GET("/block-chains/:block-data", AddBlockIntoBlockChain)

	// return http router
	return router
}
