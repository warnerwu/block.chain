package main

import (
	"net/http"
)

func main() {
	// 创建一个【区块链】
	// BlockChain := core.NewBlockChain()

	// 添加【区块】到【区块链】
	// BlockChain.SendData("The Block Index Of The 1")
	// BlockChain.SendData("The Block Index Of The 2")
	// BlockChain.SendData("The Block Index Of The 3")
	// BlockChain.Print()

	// get router register
	handler := RegisterFunctions()

	// listen port
	http.ListenAndServe(":8888", handler)
}
