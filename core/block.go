package core

import (
	"crypto/sha256"
	"encoding/hex"
	"block.chain/utils"
)

/**
 * 区块体结构定义
 */
type Block struct {
	Index        int64  // 区块编号
	Timestamp    int64  // 区块时间戳
	PerBlockHash string // 上一个区块的Hash值
	Hash         string // 当前区块的Hash值
	Data         string // 区块体数据
}

/**
 * 计算Hash值(区块编号+区块时间戳+上一个区块的Hash值+区块数据)
 *
 * @param b Block 区块结构体数据
 * @return string 返回计算Hash值
 */
func CalculateHash(b Block) string {
	// 区块编号+区块时间戳+上一个区块的Hash值+区块数据
	blockData := string(b.Index) + string(b.Timestamp) + b.PerBlockHash + b.Data

	// 计算hash值
	sum256 := sha256.Sum256([]byte(blockData))

	// 返回hash值
	return hex.EncodeToString(sum256[:])
}

/**
 * 生成创始区块(不依赖于上一个区块, 创始区块都是系统启动之前内定的)
 *
 * @return Block 返回新创建的【创始区块】
 */
func GenerateGenesisBlock() Block {
	// 定义创始区块的上一个区块(它其实是不存在的, 是虚拟的)
	preBlock := Block{}

	// 定义创始区块的上一个区块编号, 设置为【-1】这样新生成的创始区块的编号就会是【0】
	preBlock.Index = -1

	// 定义创始区块的上一个区块Hash值为空
	preBlock.Hash = ""

	// 生成【创始区块】
	return GenerateNewBlock(preBlock, "Genesis Block")
}

/**
 * 生成新区块
 *
 * @param preBlock Block 上一个区块
 * @param data string 区块体内容
 * @return Block 区块体结构数据
 */
func GenerateNewBlock(preBlock Block, data string) Block {
	// 初始化新区块
	block := Block{}

	// 新区块 - 区块编号
	block.Index = preBlock.Index + 1
	// 新区块 - 时间戳
	block.Timestamp = utils.GetTimestamp()
	// 新区块 - 上一个区块的Hash值
	block.PerBlockHash = preBlock.Hash
	// 新区块 - 区块体数据
	block.Data = data
	// 新区块 - Hash值
	block.Hash = CalculateHash(block)

	// 返回 - 新区块
	return block
}
