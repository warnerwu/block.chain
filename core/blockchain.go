package core

import (
	"github.com/labstack/gommon/log"
	"fmt"
)

/**
 * 区块链结构定义
 */
type BlockChain struct {
	Blocks []*Block
}

/**
 * 创建一个【区块链】
 *
 * @return *BlockChain 返回一个指针类型的【区块链】
 */
func NewBlockChain() *BlockChain {
	// 初始化【区块链】结构
	blockChain := &BlockChain{}

	// 创建【创始区块】
	genesisBlock := GenerateGenesisBlock()

	// 追加【创始区块】到【区块链】
	blockChain.Append(&genesisBlock)

	// 返回【区块链】
	return blockChain
}

/**
 * 区块链-发送数据生成【区块】并添加到【区块链】
 *
 * @param data string 添加到【区块链】中的【区块数据】
 */
func (bc *BlockChain) SendData(data string) {
	// 获得【区块链】的最后一个【区块】, 作为添加区块的上一个区块
	lastBlock := bc.Blocks[len(bc.Blocks)-1]
	// 生成【新区块】
	newBlock := GenerateNewBlock(*lastBlock, data)
	// 添加【新区块】到【区块链】
	bc.Append(&newBlock)
}

/**
 * 区块链-添加区块方法
 *
 * @param block *Block 区块
 */
func (bc *BlockChain) Append(block *Block) {
	if len(bc.Blocks) == 0 {
		// 追加【区块】到【区块链】
		bc.Blocks = append(bc.Blocks, block)
	} else {
		// 待追加到【区块链】有效性检测
		if bc.IsValidate(*block, *bc.Blocks[len(bc.Blocks)-1]) == false {
			log.Fatalf("Check New Block Is Invalid")
		}

		// 追加【区块】到【区块链】
		bc.Blocks = append(bc.Blocks, block)
	}
}

/**
 * 区块链-添加【区块】有效性检测
 *
 * @param block *Block 区块
 * @return bool 是否有效区块
 */
func (bc *BlockChain) IsValidate(newBlock, oldBlock Block) bool {
	// 区块编号 - 检测(新区块编号-1是否相等于上一个区块编号)
	if newBlock.Index-1 != oldBlock.Index {
		log.Printf(
			"newBlock.Index-1 != oldBlock.Index: detail[(newBlock)%d, (oldBlock)%d]",
			newBlock.Index-1,
			oldBlock.Index,
		)
		return false
	}

	// 区块Hash值 - 检测(新区块的上一个区块Hash值是否相等于上一个区块的Hash值)
	if newBlock.PerBlockHash != oldBlock.Hash {
		log.Printf(
			"newBlock.PerBlockHash != oldBlock.Hash: detail[(newBlock)%s, (oldBlock)%s]",
			newBlock.PerBlockHash,
			oldBlock.Hash,
		)
		return false
	}

	// 区块Hash值 - 检测(新区块的Hash值是否相等于Hash计算值)
	if hash := CalculateHash(newBlock); hash != newBlock.Hash {
		log.Printf(
			"newBlock.Hash != CalculateHash(newBlock): detail[(newBlock)%s, (oldBlock)%s]",
			newBlock.Hash,
			hash,
		)
		return false
	}

	return true
}

/**
 * 输出【区块链】
 */
func (bc *BlockChain) Print() {
	if len(bc.Blocks) > 0 {
		for _, block := range bc.Blocks {
			fmt.Printf("Index: %v\n", block.Index)
			fmt.Printf("Timestamp: %v\n", block.Timestamp)
			fmt.Printf("PerBlockHash: %v\n", block.PerBlockHash)
			fmt.Printf("Hash: %v\n", block.Hash)
			fmt.Printf("Data: %v\n\r\n\r", block.Data)
		}
	} else {
		fmt.Println("Block Chain Is Nil")
	}
}
